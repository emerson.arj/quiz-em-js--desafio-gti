//Nessa variável estão todas as perguntas e respostas que vocês usarão no desafio
var perguntas = [
    {
        texto: 'O céu não é o limite',
        respostas: [
            { valor: 0, texto: 'Ter preguiça' },
            { valor: 1, texto: 'Quando der certo a gente faz' },
            { valor: 3, texto: 'Vamo pra cima familia' },
            { valor: 2, texto: 'Se der for viavel a gente faz' }
        ]
    },
    {
        texto: 'Apaixonados pela missão',
        respostas: [
            { valor: 0, texto: 'Projetos nada mais sao do que numeros' },
            { valor: 1, texto: 'Dinheiro é bom e é isto' },
            { valor: 3, texto: 'Se meu projeto nao ajudar meu cliente entao nao fiz nada' },
            { valor: 2, texto: 'Se eu entender o que esta no codigo entao ta show' }
        ]
    },
    {
        texto: 'Ohana',
        respostas: [
            { valor: 0, texto: 'Ta com problema? Se vira filhao' },
            { valor: 3, texto: 'Voce é meu irmao de batalha, to contigo e nao solto' },
            { valor: 2, texto: 'Voce é da gti, eu tambem entao tudo certo' },
            { valor: 1, texto: 'Fora da Gti nem olho' }
        ]
    },
    {
        texto: 'Integridade',
        respostas: [
            { valor: 0, texto: 'Furar a fila do ru' },
            { valor: 2, texto: 'farol amarelo pra mim é verde' },
            { valor: 1, texto: 'Clonar cartao de credito' },
            { valor: 3, texto: 'Sou integro' }
        ]
    },
    {
        texto: 'Ser capitão da nave',
        respostas: [
            { valor: 3, texto: 'Eu serei o lider que eu quero pra mim' },
            { valor: 0, texto: 'Eu nao sabia, entao nao fiz' },
            { valor: 1, texto: 'Ve la, depois me conta' },
            { valor: 2, texto: 'To so esperando meu estagio aprovar pra sair da GTi' }
        ]
    },
    {
        texto: 'Quem é o dono da GTi?',
        respostas: [
            { valor: 2, texto: 'É O LIMA ' },
            { valor: 0, texto: 'É O ROCHA' },
            { valor: 1, texto: 'É O KLEYTON' },
            { valor: 3, texto: 'OS MEMBROS' }
        ]
    },

]
//--------------------------------------VARIÁVEIS---------------------------------------------//

// contador que será usado para as perguntas
var i=0;
// variável que representao o começo do jogo (primeira vez que o botão é clickado)
var primeiro = true;
// variável com a pontuação do quiz
var pontos = 0;

// -----------------------------------FUNÇÃO PRINCIPAL------------------------------------------------//
function mostrarQuestao() {

     // vetor com os imputs
    var escolhas = document.getElementsByName("resposta");

    // assim que o jogo começa ele já incrementa 1 no i, então usaremos essa variável para checkar as respostas,
    // pois primeiro as opções são carregadas, e so após o usuário apertar o botão "Próximo" novamente é que é checkado qual item ele marcou.
    var x =i-1;

    // verifica se o algum imput foi marcado ou se é o primeiro loop
    if( escolhas[0].checked == true || escolhas[1].checked == true ||  escolhas[2].checked == true || escolhas[3].checked == true || primeiro == true){
         
        //mostra a lista com os imputs
        document.getElementById('listaRespostas').style.display = 'block';   

        // vetor que representa os spans onde as perguntas ficam, ao lado dos imputs
       var lista =  document.getElementsByTagName("span");
      
//---------------------------------------código que será executado até a penultima vez que o botão "Próximo" for clickado----------------------//
        if(i<perguntas.length){
            //altera o título para o título da pergunta
        document.getElementById("titulo").innerHTML= perguntas[i].texto;
            // altera o texto do botão
        document.getElementById("confirmar").innerHTML= "Próximo";

        // passa o texto das respostas para os spans
      for(var y=0;y<lista.length;y++){
        
       lista[y].innerHTML=perguntas[i].respostas[y].texto;
      }

       //verificando os imputs que foram marcados
      for(var z=0;z<4;z++){
          
          if(escolhas[z].checked==true){
                pontos = pontos + (perguntas[x].respostas[z].valor);
          }
      }
      //resetando os imputs
      for(var w=0;w<4;w++){
        
        escolhas[w].checked = false;
    }
     // incrementa o i    
     i++;
        }
//---------------------- código que será executado quando botão "Próximo" for clickado na ultima pergunta--------------------------------//
        else if(i==perguntas.length){
            // verifica o imput marcado para  a ultima opção
            for(var z=0;z<4;z++){
          
                if(escolhas[z].checked==true){
                    // soma a variável pontos com o valor correspondente para o opção marcada
                      pontos = pontos + (perguntas[x].respostas[z].valor);
                }
            }
            //renomeia o botão 
            document.getElementById("confirmar").innerHTML= "Recomeçar";
            //some com a lista de perguntas
            document.getElementById("listaRespostas").style.display = "none";
           // altera o titulo 
            document.getElementById("titulo").innerHTML = "QUIZ DOS VALORES DA GTI";
            //resultado em % , a soma máxima dos pontos é perguntas.lenght x 3
            var maxPont = perguntas.length*3;
            //calculo da pontuação
            var pontuacao = (pontos/maxPont)*100 ;
            // mostra a porcentagem, usei o toFixed paralimitar as casas decimais
            document.getElementById("resultado").innerHTML ="RESULTADO : "+pontuacao.toFixed(2) + "%";
             // incrementa o i    
             i++;
      
        }
//------------------------------- código que será executado caso o usuário clicke no botão após o fim do quiz (RECOMEÇAR)-------------------------//
        else if(i==(perguntas.length)+1){
            //limpa o resultado
            document.getElementById("resultado").innerHTML ="";
            //zera os pontos
            pontos = 0;
            // começo do jogo
            primeiro = true;
            //resetando os imputs
            for(var w=0;w<4;w++){
        
                escolhas[w].checked = false;
            }      
            i=0;
            //atualizando tudo para recomeçar com os valores adequados para a primeira questão
            mostrarQuestao();
          
        }

    // primeiro loop acaba
    primeiro = false;
    }
}
